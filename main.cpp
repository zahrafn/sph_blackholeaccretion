//
// Created by Zahra Forootaninia on 12/3/15.
/*
 * In this you can compile to exmaples and also by changinf the parameters such as magnitude of B field (b_magnetic) or mass of black hole (bh_mass )
 * you can get diffrenet cases.
 *
 * have to note that all unites are in solar unites.
 *
 * importanpe parameters to play with :
 *
 *              size_p = numebr of particles
 *              bh_mass = mass of the black hole
 *              b_magnetic = magnitude of magnetic field
 *              m_scale = switch for force from magnetic field (0--> off or 1-->on)
 *              p_scale = switch for force from pressure (0--> off or 1-->on)
 *              gSelf_scale = switch for self gravity force (0--> off or 1-->on)
 *              gCenter_scale = switch for centranl force from balck hole (0--> off or 1-->on)
 *              v_scale = swith for viscosity force (0--> off or 1-->on)
 *
*/



#include <iostream>
#include "main.h"
#include "camera.h"
#include <math.h>
#include "kernels.h"
#include "Particle.h"
#include "opengl.h"
#include "gl_util.hpp"
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */




const int size_p = 500;                                           // number of particles
const int max_segments = 1000000;                                 // maximun number of segments for tracinf particles in the jet

const float g_const = 1.0; //6.67408e-11; m3 kg-1 s-2 ;          //
const float bh_mass = 10.0;                                      // mass of the black hole in solar unit example: 10 M(sun)
const float bh_r = 10.0f;                                        // raduise of the black hole

const float b_magnetic = 10.0;                                   // magnetic field strenght
const float magnetic_r = 0.1;                                    // raduise where farticles feel magnetic force

const float v_decay = 0.5;                                       // decay in particles orbit to fall in the BH

//// for BH example
const float inner_radius = 0.4;                                  // inner radiuse of accretion disk (solar unit )
const float outter_radius = 1.2;                                 // outter radiuse of accretion disk
const float thickness = 0.3;

// for steam of partiles exmp 1
//const float inner_radius = 0.5;
//const float outter_radius = 0.5;
//const float thickness = 0.5;

// Scales : m-> magnetic , p-> hydro , pCenter-> centeral pressure, gSelf-> selfgarvity, gCenter-> centeral garvity, v-> viscosity

const float m_scale = 1.0;                                      //switch for force from magnetic field (0--> off or 1-->on)
const float p_scale = 1.0;                                      //switch for force from pressure (0--> off or 1-->on)
const float gSelf_scale = 0.0;                                  //switch for self gravity force (0--> off or 1-->on)
const float gCenter_scale = 1.0;                                ////switch for centranl force from balck hole (0--> off or 1-->on)
const float v_scale = 1.0;                                      //swith for viscosity force (0--> off or 1-->on)

const float max_vel = 8.0;                                     //maximum velocity
const float y_offset = 1.0;                                    //offset in y postion for tracing

// particles properties

Particle particles[size_p];
float current_force[size_p][3];
float new_force[size_p][3];
float dt;
float trace[max_segments][6];

float vel_color[size_p];                                     //color of porticles based on there velocity

int current_segment = 0;
int num_segment = 0;

int frame_number = 0;


using namespace std;




void particle_init(Particle particle_array[], int size_p) {


    float x_limit = 1.0f;
    srand (4);


    for (int i = 0; i < size_p; i++) {

        particle_array[i].m = 1.0f;
        particle_array[i].h = 0.06f;
        particle_array[i].q = 1.0f;
        particle_array[i].dead = false;
        particle_array[i].trace_flag = false;

        for (int j = 0; j < 3; ++j) {

            particle_array[i].v[j] = 0.0f;
            particle_array[i].r[j] = 0.0f;

            new_force[i][j] = 0.0f;
            current_force[i][j] = 0.0f;

        }

        // In the Black Hole case using Disk Point Picking for position of partilces (lines 120 -135)

        float t = 2 * M_PI * (rand()/ float(RAND_MAX))+1;
        float r = outter_radius * ((rand()/ float(RAND_MAX)) + inner_radius);

        particle_array[i].r[0] = sin(t) * r;
        particle_array[i].r[1] = cos(t) * r;
        particle_array[i].r[2] = thickness * ((rand()/ float(RAND_MAX))-0.5);

        float dist = sqrt( pow( particle_array[i].r[0], 2) + pow( particle_array[i].r[1], 2));
        float orbital_v = v_decay * sqrt(g_const * bh_mass / dist ) / dist;

        for(int j = 0; j < 3; ++j){

            particle_array[i].v[0] = - particle_array[i].r[1] * orbital_v;
            particle_array[i].v[1] = particle_array[i].r[0] * orbital_v;

        }

        // for stream of particles case use below position setting (lines 139 -145)

//            particle_array[i].r[0] = 0.2 * (rand() / float(RAND_MAX))- 0.5;
//            particle_array[i].r[1] = 0.2 * (rand() / float(RAND_MAX));
//            particle_array[i].r[2] = 0.5 * (rand() / float(RAND_MAX)) - 2;
//
//            particle_array[i].v[0] = 2.0;
//            particle_array[i].v[1] = 5.0;
//            particle_array[i].v[2] = 2.0;




    }
}


void particles_density(Particle particle_array[], int size_p) {

    for (int i = 0; i < size_p; i++) {

        particle_array[i].rho = 0.0f;

        float rk_array[3] = {0.0f, 0.0f, 0.0f};

        for (int j = 0; j < size_p; j++) {

            for (int k = 0; k < 3; k++) {

                rk_array[k] = particle_array[i].r[k] - particle_array[j].r[k];
            }
            float rk = sqrt(pow(rk_array[0], 2) + pow(rk_array[1], 2) + pow(rk_array[2], 2));

            particle_array[i].rho += particle_array[j].m * csplinkernel(rk, particle_array[j].h);

        }
    }
}


void pressure(Particle particle_array[] , int size_p){

    float const_k = 8.3144598;	//J K−1 mol−1
    float rho_0 = 0.0f;

    for(int i = 0; i < size_p; i++) {
        particle_array[i].p = const_k * (particle_array[i].rho - rho_0);
    }
}


// B_r, B_theta, B_z should be 1 if they exsist and 0 if not.

void get_magnetic_force(Particle particle_array[],int p, float b_mag_shape, int B_r, int B_theta, int B_z, int size_p, float magnetic_force[3]){

    float theta = atan2( particle_array[p].r[1], particle_array[p].r[0] );
    float b_mag = particle_array[p].q * b_magnetic / b_mag_shape ;

    float mforce_r[3] = {0.0f, 0.0f, 0.0f};
    float mforce_theta[3] = {0.0f, 0.0f, 0.0f};
    float mforce_z[3] = {0.0f, 0.0f, 0.0f};


    if(B_r == 1) {

        mforce_r[0] = - b_mag * particle_array[p].v[2] * sin(theta);
        mforce_r[1] = b_mag * particle_array[p].v[2] * cos(theta);
        mforce_r[2] = b_mag * (particle_array[p].v[1] * cos(theta) - particle_array[p].v[0] * sin(theta));

    }

    if(B_theta == 1){

        mforce_theta[0] = - b_mag * particle_array[p].v[2] * cos(theta);
        mforce_theta[1] = - b_mag * particle_array[p].v[2] * sin(theta);
        mforce_theta[2] = b_mag * (particle_array[p].v[0] * cos(theta) + particle_array[p].v[1] * sin(theta));

    }

    if(B_z == 1){

        mforce_z[0] = b_mag * particle_array[p].v[1];
        mforce_z[1] = - b_mag * particle_array[p].v[0];
        mforce_z[2] = 0.0;

    }

    magnetic_force[0] = B_r * mforce_r[0] + B_theta * mforce_theta[0] + B_z * mforce_z[0];
    magnetic_force[1] = B_r * mforce_r[1] + B_theta * mforce_theta[1] + B_z * mforce_z[1];
    magnetic_force[2] = B_r * mforce_r[2] + B_theta * mforce_theta[2] + B_z * mforce_z[2];


}



void compute_forces(Particle particle_array[], int size_p){

    float mu = 0.01f;
    float dist = 0.0f;
    float c = 8.3144598; // from taking drivitive of equation of state give us c which is const_k
    dt = 0.01 / c;

    for(int i = 0; i < size_p; i++) {

        float pforce[3] = {0.0f, 0.0f, 0.0f};
        float vforce[3] = {0.0f, 0.0f, 0.0f};
        float gforce[3] = {0.0f, 0.0f, 0.0f};
        float cforce[3] = {0.0f, 0.0f, 0.0f};
        float cpforce[3] = {0.0f, 0.0f, 0.0f};
        float mforce[3] = {0.0f, 0.0f, 0.0f};

        // ********** distance of particle to center
        float rc_array[3] = {0.0f, 0.0f, 0.0f};

        for (int k = 0; k < 3; k++) {
            rc_array[k] = particle_array[i].r[k];
        }

        float xy_sqr = pow(rc_array[0], 2) + pow(rc_array[1], 2);
        float z_sqr = pow(rc_array[2], 2);
        float rc_sqr = xy_sqr + z_sqr;
        float rc = sqrt(rc_sqr);

        // unit vector for centeral force
        for (int k = 0; k < 3; k++) {
            rc_array[k] /= (rc + 1e-6);
        }

        // ************* centeral froce from BH ***************

        for (int k = 0; k < 3; k++) {
            cforce[k] += rc_array[k] * (bh_mass / rc_sqr);

        }
        // ***************************************************

        // ************* magnetic froce (either 1 or 2) ***************
        // ******************* (1)  In the Black Hole case: we have B feild in r direction only

        float b_mag_shape_BH = pow(xy_sqr + 0.0 * z_sqr, 2);

        if(particle_array[i].r[2] < 0.0) {
            b_mag_shape_BH = -b_mag_shape_BH;
        }
        if(xy_sqr < magnetic_r) {
            get_magnetic_force(particle_array ,i , b_mag_shape_BH, 1, 0, 0, size_p, mforce);
        }
        else{
            mforce[0] = mforce[1] = mforce[2] = 0.0;
        }

        // ********************************************************



       // ******************* (2) computing magnetic force for stream of particles in magnetic feild

//        float b_mag_shape_exmp1 =1.0; /// pow(xy_sqr , 2);
//        get_magnetic_force(particle_array, i, b_mag_shape_exmp1, 0, 0, 1, size_p, mforce);

    //********************************************************


        // Forces on particle i due to particle j
        for (int j = 0; j < size_p; j++) {

            if (i == j) continue;

            float rk_array[3] = {0.0f, 0.0f, 0.0f};


            // ********** distance of particles to each other
            for (int k = 0; k < 3; k++) {
                rk_array[k] = particle_array[i].r[k] - particle_array[j].r[k];
            }

            float rk = sqrt(pow(rk_array[0], 2) + pow(rk_array[1], 2) + pow(rk_array[2], 2));
            // **********

            // unit vector
            for (int k = 0; k < 3; k++) {
                rk_array[k] /= (rk + 1e-6);
            }

            for (int k = 0; k < 3; k++) {

                // ******************* computing pressure force

                pforce[k] += -rk_array[k] * (particle_array[j].m * (particle_array[i].p + particle_array[j].p)) / (2.0 * particle_array[j].rho) * (first_driv_csplinkernel(rk, particle_array[j].h));

                // ******************* computing viscosity force

                vforce[k] += (particle_array[j].m * (particle_array[j].v[k] - particle_array[i].v[k])) / (particle_array[j].rho) * (second_driv_csplinkernel(rk, particle_array[j].h));

                // ******************* computing gravitational force

                gforce[k] += rk_array[k] * pow(particle_array[j].m, 2) / pow(rk, 2);

            }
        }



        // ********************* total force for each particle

        for (int k = 0; k < 3; k++) {
            new_force[i][k] = p_scale * pforce[k] + v_scale * mu * vforce[k] - gSelf_scale * g_const * gforce[k] -  gCenter_scale * g_const * cforce[k]  + m_scale * mforce[k] ;
        }

        float forces = sqrt( pow(new_force[i][0],2) +  pow(new_force[i][1],2) +  pow(new_force[i][2],2) );

        // ******************* computing spead of sound

        float new_dt = 0.1 * sqrt(particle_array[i].h / (forces + 1e-10));

        dt = 1.0 * std::max(std::min(dt, new_dt),1.0e-3f);

        // ********************* kill particles after they get out of the frame and put them randomly in the disk again

        if(rc > 2.0){

            particle_array[i].dead = true;
            particle_array[i].trace_flag = false;

        }
        float tot_magnetic = pow(mforce[0],2)+pow(mforce[1],2)+pow(mforce[2],2);

        if(tot_magnetic > 0.0) {
            particle_array[i].trace_flag = true;
        }

    }
}



// ******************* leap frog inegration *******************

void time_step(Particle particle_array[] , float dt, int p_size){


    for (int i = 0; i < p_size; i++) {

        if(particle_array[i].trace_flag) {

            trace[current_segment][0] = particle_array[i].r[0];
            trace[current_segment][1] = particle_array[i].r[1] + y_offset;
            trace[current_segment][2] = particle_array[i].r[2];
        }

        for (int k = 0; k < 3; k++) {



            particle_array[i].r[k] = particle_array[i].r[k] + particle_array[i].v[k] * dt + 0.5 * current_force[i][k] * pow(dt, 2);
            particle_array[i].v[k] = particle_array[i].v[k] + 0.5 * (current_force[i][k] + new_force[i][k]) * dt;
            current_force[i][k] = new_force[i][k];

            // puting dead particles back to the disk BH (only for BH case comment out for stream exmaple)
            if(particle_array[i].dead){
                current_force[i][k] = 0.0;
                float t = 2 * M_PI * (rand()/ float(RAND_MAX))+1;
                float r = 0.7 * ((rand()/ float(RAND_MAX)) + outter_radius );

                particle_array[i].r[0] = sin(t) * r;
                particle_array[i].r[1] = cos(t) * r ;
                particle_array[i].r[2] = thickness * ((rand()/ float(RAND_MAX))-0.5);
                particle_array[i].dead = false;

                float dist = sqrt( pow( particle_array[i].r[0], 2) + pow( particle_array[i].r[1], 2));
                float orbital_v = 0.8 * sqrt(g_const * bh_mass / dist ) / dist;

                for(int j = 0; j < 3; ++j){

                    particle_array[i].v[0] = - particle_array[i].r[1] * orbital_v;
                    particle_array[i].v[1] = particle_array[i].r[0] * orbital_v;
                    particle_array[i].v[2] = 0.0;
                }
            }

        }
        //  consrain to make particles not shotting off crazy since we don't have relativity here

        float total_vel = sqrt(pow(particle_array[i].v[0],2) + pow(particle_array[i].v[1],2) + pow(particle_array[i].v[2],2));

        if(total_vel > max_vel ){
            particle_array[i].v[0] = max_vel * particle_array[i].v[0] / total_vel;
            particle_array[i].v[1] = max_vel * particle_array[i].v[1] / total_vel;
            particle_array[i].v[2] = max_vel * particle_array[i].v[2] / total_vel;
        }


        vel_color[i] = total_vel;


        // If the condition above was met we also need to do this here
        if(particle_array[i].trace_flag) {

            trace[current_segment][3] = particle_array[i].r[0];
            trace[current_segment][4] = particle_array[i].r[1] + y_offset;
            trace[current_segment][5] = particle_array[i].r[2];

            ++current_segment;
            ++num_segment;

            if (current_segment >= max_segments) {
                current_segment -= max_segments;
            }
            if(num_segment > max_segments) {
                num_segment = max_segments;
            }
        }
    }
}



void advanceSimulation(){

    particles_density(particles, size_p);

    pressure(particles, size_p);

    compute_forces(particles, size_p);

    time_step(particles, dt, size_p);

}





// ########################################################################## using Spheres

int w = 800, h = 800; // size of window in pixels
Camera camera;        // camera

struct MouseInfo {
    bool dragging;    // is mouse being dragged?
    int xdrag, ydrag; // where the drag began
    int xprev, yprev; // where the mouse was last seen
} mouseInfo;

void drawCube() {
    glPushAttrib(GL_LIGHTING_BIT);
    glDisable(GL_LIGHTING);
    glColor3f(0.6,0.6,0.6);
    glBegin(GL_LINES);
    double x[] = {-1, 1}, y[] = {0, 2}, z[] = {-1, 1};
    for (int j = 0; j < 2; j++) {
        for (int k = 0; k < 2; k++) {
            glVertex3f(x[0], y[j], z[k]);
            glVertex3f(x[1], y[j], z[k]);
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int k = 0; k < 2; k++) {
            glVertex3f(x[i], y[0], z[k]);
            glVertex3f(x[i], y[1], z[k]);
        }
    }
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            glVertex3f(x[i], y[j], z[0]);
            glVertex3f(x[i], y[j], z[1]);
        }
    }
    glEnd();
    glPopAttrib();
}


void drawPoints() {

    glPointSize(5);
    glColor3f(0,0,0);
    glBegin(GL_POINTS);
    for (int p = 0; p < size_p; p++) {
        glVertex2f(particles[p].r[0], particles[p].r[1]);
    }
    glEnd();
}
void drawSpheres() {



    // Draw BH for BH case
    glColor3f(0.0, 0.0, 0.0);
    glPushMatrix();
    glTranslatef(0.0f, y_offset, 0.0f);
    glutSolidSphere(0.1, 20, 20);
    glPopMatrix();

    // Draw SPH particles
    for (int s = 0; s < size_p; s++) {
        float c = min(1.0, 0.8 * log10(3.0 * vel_color[s] + 0.1));
        glColor3f( c, 0.5 * c,0.1 * log10(fabs(particles[s].v[2]) + 1.0));
        glPushMatrix();
        glTranslatef(particles[s].r[0], y_offset + particles[s].r[1], particles[s].r[2]);
        glutSolidSphere( particles[s].h, 10, 10);
        glPopMatrix();

    }

    // draw trace lines
    glDisable(GL_LIGHTING);
    glColor3f(0.137255, 0.137255, 0.556863); //navy
    glEnableClientState( GL_VERTEX_ARRAY );
    glVertexPointer( 3, GL_FLOAT, 0, trace );
    glDrawArrays( GL_LINES, 0, 2. * num_segment );
    glDisableClientState( GL_VERTEX_ARRAY );
    glEnable(GL_LIGHTING);

}


void display() {

    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_NORMALIZE);

    camera.apply();

    drawCube();
    drawSpheres();
    glutSwapBuffers();

    // taking screan shots of the frame every 2 frames
    if (frame_number % 2 == 0) {
        ScreenShot screen_shot;
        screen_shot.write("sph_W_B_500p", frame_number / 2, w, h);
    }

    ++frame_number;
}

void reshape(int w, int h) {
    ::w = w;
    ::h = h;
    glViewport(0, 0, w, h);
}

void idle() {
    advanceSimulation();
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) {
    ::mouseInfo.xprev = x;
    ::mouseInfo.yprev = y;
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            ::mouseInfo.dragging = true;
            ::mouseInfo.xdrag = x;
            ::mouseInfo.ydrag = y;
        } else if (state == GLUT_UP) {
            ::mouseInfo.dragging = false;
        }
    }
}

void motion(int x, int y) {
    camera.longitude -= (x - ::mouseInfo.xprev)*0.2;
    camera.latitude += (y - ::mouseInfo.yprev)*0.2;
    camera.latitude = min(max(camera.latitude, -90.), 90.);
    ::mouseInfo.xprev = x;
    ::mouseInfo.yprev = y;
}




// ################################################ Main ################################################

int main(int argc, char **argv) {

    particle_init(particles, size_p);

    camera = Camera(Vector3d(0,1,0), 6, 30);

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH|GLUT_MULTISAMPLE);
    glutInitWindowSize(::w, ::h);
    glutCreateWindow("Animation");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutMainLoop();

    return 0;

}
