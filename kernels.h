//
// Created by Zahra Forootaninia on 11/17/15.
//

#ifndef ANIMATION_FINALPROJ_KERNAL_H
#define ANIMATION_FINALPROJ_KERNAL_H

float csplinkernel(float r, float h);

float first_driv_csplinkernel(float r, float h);

float second_driv_csplinkernel(float r, float h);


#endif //ANIMATION_FINALPROJ_KERNAL_H
