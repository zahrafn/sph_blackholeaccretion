/*
 * gl_util.cpp
 *
 *  Created on: Nov 24, 2011
 *      Author: Stou Sandalski (stou@icapsid.net)
 *     License: Apache 2.0
 *
 * Description:
 *
 */

// Used this calss for taking screan shots of the frames

#ifndef HPP_NEWBERRY_GL_UTIL
#define HPP_NEWBERRY_GL_UTIL

#if __APPLE__ & __MACH__
#include <GLUT/glut.h>
#elif defined(_WIN32)
#include <Windows.h>
#include <GL/freeglut.h>
#else
#include <GL/freeglut.h>
#endif


#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <png.h>
#include <zlib.h>


class ScreenShot {

private:
	png_structp m_png_ptr;
	png_infop m_info_ptr;

public:
	ScreenShot() {
		m_png_ptr = 0;
		m_info_ptr = 0;
	}

	~ScreenShot() {
		if ((m_png_ptr) && (m_info_ptr)) {
//  		png_destroy_write_struct(&m_png_ptr, &m_info_ptr);
		}
	}

	bool write(const char *prefix, int frame, int width, int height, bool alpha = false) {

		using namespace std;

		std::stringstream ss;
		ss << prefix << "_" << setw(4) << setfill('0') << frame << ".png";

		cout <<"Saving to "<<ss.str().c_str()<< endl;

		GLubyte pixels[4 * width * height];

//    GLint m_viewport[4];
//    glGetIntegerv( GL_VIEWPORT, m_viewport );
//    cout<<"View port "<<m_viewport[2]<<", "<<m_viewport[3]<<endl;

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
//    glReadBuffer(GL_FRONT);

		if (alpha) {
			glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		} else {
			glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixels);
		}

		FILE *m_fp = fopen(ss.str().c_str(), "wb");

		if (!m_fp) {
			return false;
		}

		m_png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, 0, 0);

		if (!m_png_ptr) {
			return false;
		}

		m_info_ptr = png_create_info_struct(m_png_ptr);

		if (!m_info_ptr) {
			png_destroy_write_struct(&m_png_ptr, (png_infopp) NULL);
			fclose(m_fp);
			return false;
		}

		if (setjmp(png_jmpbuf(m_png_ptr))) {
			png_destroy_write_struct(&m_png_ptr, &m_info_ptr);
			fclose(m_fp);
			return false;
		}

		png_init_io(m_png_ptr, m_fp);

		if (!m_info_ptr) {
			return false;
		}

		png_bytep screen[height];

		int channels = alpha ? 4 : 3;

		for (int i = 0; i < height; ++i) {
			screen[i] = &pixels[channels * width * (height - i - 1)];
		}

		png_time t;
		png_convert_from_time_t(&t, time(0));
		png_set_tIME(m_png_ptr, m_info_ptr, &t);

		png_set_compression_level(m_png_ptr, Z_BEST_COMPRESSION);

		if (alpha) {
			png_set_IHDR(m_png_ptr, m_info_ptr, width, height, 8,
						 PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
						 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
		} else {
			png_set_IHDR(m_png_ptr, m_info_ptr, width, height, 8,
						 PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
						 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
		}

		png_write_info(m_png_ptr, m_info_ptr);
		png_write_image(m_png_ptr, screen);
		png_write_end(m_png_ptr, m_info_ptr);

		fclose(m_fp);

		return true;
	}

};

#endif // HPP_NEWBERRY_GL_UTIL
