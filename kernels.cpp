//
// Created by Zahra Forootaninia on 11/17/15.
//

#include <math.h>

#include "kernels.h"

/*
 * Here kernal function and it's second and third derivative computed to use for finding density and forces
 */

float csplinkernel(float r, float h){

    float q = fabs(r / h);
    float sig = 2./3.;   // 2/3 --> 1D,  10/(7π)-->2D ,1/π --> 3D
    float cspline;

    //std::cout<<"q is in spline: "<< q << "\n";
    if((q >= 0.0) && (q<1.0)){
        cspline =sig *( 1 - ((3./2.) * pow(q,2)) + ((3./4.) * pow(q,3)));
    }
    else if((q>=1.) && (q<2.)){
        cspline =  sig *((1./4.) * pow((2.- q),3));
    }
    else if(q>=2.) {
        cspline = 0;
    }
    return cspline;

}

float first_driv_csplinkernel(float r, float h){

    float q = fabs(r / h);
    float sig = 2./3.;   // 2/3 --> 1D,  10/(7π)-->2D ,1/π --> 3D
    float d_cspline;

    if((q >= 0.0) && (q<1.0)){
        d_cspline =sig *((- 3.) * q + ((9./4.) * pow(q,2)));
    }
    else if((q>=1.) && (q<2.)){
        d_cspline =  sig *((3./4.) * pow((2.- q),2));
    }
    else if(q>=2.) {
        d_cspline = 0;
    }
    return d_cspline;

}

float second_driv_csplinkernel(float r, float h){

    float q = fabs(r / h);
    float sig = 2./3.;   // 2/3 --> 1D,  10/(7π)-->2D ,1/π --> 3D
    float d_cspline;

    if((q >= 0.0) && (q<1.0)){
        d_cspline =sig *((- 3.) + ((9./2.) * q));
    }
    else if((q>=1.) && (q<2.)){
        d_cspline =  sig *((3./2.) * (2.- q));
    }
    else if(q>=2.) {
        d_cspline = 0;
    }
    return d_cspline;

}

