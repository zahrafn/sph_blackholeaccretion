//
// Created by Zahra Forootaninia on 11/17/15.
//

#ifndef ANIMATION_FINALPROJ_PARTICLE_H
#define ANIMATION_FINALPROJ_PARTICLE_H


struct Particle{

    float m;         // mass of particle
    float h;         //smoothing lenhgt
    float p;         //pressure at the position of the particle
    float rho;      //density at the position of the particle
    float q;        //particles's charge
    float v[3];     //particle's velocity
    float r[3];     //particle's position
    bool dead;      //when the particle get out of the frame I falg it as dead
    bool trace_flag;//particle's getting into ject set as trace so I trace them with lines

};

#endif //ANIMATION_FINALPROJ_PARTICLE_H
